package starts;import objects.*;
import rules.*;
import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.annotation.Rule;
import org.easyrules.api.RulesEngine;
import org.easyrules.core.RulesEngineBuilder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import static starts.Measurer.cpu;
import static starts.Measurer.memoryUsage;
import static starts.Measurer.processorUsage;

public class Start{
    //This file is your base class, containing rules engine object, and all rules, to fire instantiate this class in other file and write class_name.rulesEngine.fire()
    public RulesEngine rulesEngine;

    //Class Objects
    public ocenakandydata _ocenakandydata;
    //Rules Objects
    public ocenakandydata_1 _ocenakandydata_1;
    public ocenakandydata_2 _ocenakandydata_2;
    public ocenakandydata_3 _ocenakandydata_3;
    public ocenakandydata_4 _ocenakandydata_4;
    public ocenakandydata_5 _ocenakandydata_5;
    public ocenakandydata_6 _ocenakandydata_6;
    public ocenakandydata_7 _ocenakandydata_7;
    public ocenakandydata_8 _ocenakandydata_8;
    public ocenakandydata_9 _ocenakandydata_9;
    public ocenakandydata_10 _ocenakandydata_10;
    public ocenakandydata_11 _ocenakandydata_11;
    public ocenakandydata_12 _ocenakandydata_12;
    public ocenakandydata_13 _ocenakandydata_13;
    public ocenakandydata_14 _ocenakandydata_14;
    public ocenakandydata_15 _ocenakandydata_15;
    public ocenakandydata_16 _ocenakandydata_16;
    public ocenakandydata_17 _ocenakandydata_17;
    public ocenakandydata_18 _ocenakandydata_18;
    public ocenakandydata_19 _ocenakandydata_19;
    public ocenakandydata_20 _ocenakandydata_20;
    public ocenakandydata_21 _ocenakandydata_21;
    public ocenakandydata_22 _ocenakandydata_22;
    public ocenakandydata_23 _ocenakandydata_23;
    public ocenakandydata_24 _ocenakandydata_24;
    public ocenakandydata_25 _ocenakandydata_25;
    public ocenakandydata_26 _ocenakandydata_26;
    public ocenakandydata_27 _ocenakandydata_27;
    public ocenakandydata_28 _ocenakandydata_28;
    public ocenakandydata_29 _ocenakandydata_29;
    public ocenakandydata_30 _ocenakandydata_30;
    public ocenakandydata_31 _ocenakandydata_31;
    public ocenakandydata_32 _ocenakandydata_32;
    public ocenakandydata_33 _ocenakandydata_33;
    public ocenakandydata_34 _ocenakandydata_34;
    public ocenakandydata_35 _ocenakandydata_35;
    public ocenakandydata_36 _ocenakandydata_36;
    public ocenakandydata_37 _ocenakandydata_37;
    public ocenakandydata_38 _ocenakandydata_38;
    public ocenakandydata_39 _ocenakandydata_39;
    public ocenakandydata_40 _ocenakandydata_40;
    public ocenakandydata_41 _ocenakandydata_41;
    public ocenakandydata_42 _ocenakandydata_42;
    public ocenakandydata_43 _ocenakandydata_43;
    public ocenakandydata_44 _ocenakandydata_44;
    public ocenakandydata_45 _ocenakandydata_45;
    public ocenakandydata_46 _ocenakandydata_46;
    public ocenakandydata_47 _ocenakandydata_47;
    public ocenakandydata_48 _ocenakandydata_48;
    public ocenakandydata_49 _ocenakandydata_49;
    public ocenakandydata_50 _ocenakandydata_50;
    public ocenakandydata_51 _ocenakandydata_51;
    public ocenakandydata_52 _ocenakandydata_52;
    public ocenakandydata_53 _ocenakandydata_53;
    public ocenakandydata_54 _ocenakandydata_54;
    public ocenakandydata_55 _ocenakandydata_55;
    public ocenakandydata_56 _ocenakandydata_56;
    public ocenakandydata_57 _ocenakandydata_57;
    public ocenakandydata_58 _ocenakandydata_58;
    public ocenakandydata_59 _ocenakandydata_59;
    public ocenakandydata_60 _ocenakandydata_60;
    public ocenakandydata_61 _ocenakandydata_61;
    public ocenakandydata_62 _ocenakandydata_62;
    public ocenakandydata_63 _ocenakandydata_63;
    public ocenakandydata_64 _ocenakandydata_64;
    public Start(){
        rulesEngine = RulesEngineBuilder.aNewRulesEngine().build();
//Class Objects
        _ocenakandydata = new ocenakandydata();
//Rules Objects
        _ocenakandydata_1 = new ocenakandydata_1(this);
        rulesEngine.registerRule(this._ocenakandydata_1);
        _ocenakandydata_2 = new ocenakandydata_2(this);
        rulesEngine.registerRule(this._ocenakandydata_2);
        _ocenakandydata_3 = new ocenakandydata_3(this);
        rulesEngine.registerRule(this._ocenakandydata_3);
        _ocenakandydata_4 = new ocenakandydata_4(this);
        rulesEngine.registerRule(this._ocenakandydata_4);
        _ocenakandydata_5 = new ocenakandydata_5(this);
        rulesEngine.registerRule(this._ocenakandydata_5);
        _ocenakandydata_6 = new ocenakandydata_6(this);
        rulesEngine.registerRule(this._ocenakandydata_6);
        _ocenakandydata_7 = new ocenakandydata_7(this);
        rulesEngine.registerRule(this._ocenakandydata_7);
        _ocenakandydata_8 = new ocenakandydata_8(this);
        rulesEngine.registerRule(this._ocenakandydata_8);
        _ocenakandydata_9 = new ocenakandydata_9(this);
        rulesEngine.registerRule(this._ocenakandydata_9);
        _ocenakandydata_10 = new ocenakandydata_10(this);
        rulesEngine.registerRule(this._ocenakandydata_10);
        _ocenakandydata_11 = new ocenakandydata_11(this);
        rulesEngine.registerRule(this._ocenakandydata_11);
        _ocenakandydata_12 = new ocenakandydata_12(this);
        rulesEngine.registerRule(this._ocenakandydata_12);
        _ocenakandydata_13 = new ocenakandydata_13(this);
        rulesEngine.registerRule(this._ocenakandydata_13);
        _ocenakandydata_14 = new ocenakandydata_14(this);
        rulesEngine.registerRule(this._ocenakandydata_14);
        _ocenakandydata_15 = new ocenakandydata_15(this);
        rulesEngine.registerRule(this._ocenakandydata_15);
        _ocenakandydata_16 = new ocenakandydata_16(this);
        rulesEngine.registerRule(this._ocenakandydata_16);
        _ocenakandydata_17 = new ocenakandydata_17(this);
        rulesEngine.registerRule(this._ocenakandydata_17);
        _ocenakandydata_18 = new ocenakandydata_18(this);
        rulesEngine.registerRule(this._ocenakandydata_18);
        _ocenakandydata_19 = new ocenakandydata_19(this);
        rulesEngine.registerRule(this._ocenakandydata_19);
        _ocenakandydata_20 = new ocenakandydata_20(this);
        rulesEngine.registerRule(this._ocenakandydata_20);
        _ocenakandydata_21 = new ocenakandydata_21(this);
        rulesEngine.registerRule(this._ocenakandydata_21);
        _ocenakandydata_22 = new ocenakandydata_22(this);
        rulesEngine.registerRule(this._ocenakandydata_22);
        _ocenakandydata_23 = new ocenakandydata_23(this);
        rulesEngine.registerRule(this._ocenakandydata_23);
        _ocenakandydata_24 = new ocenakandydata_24(this);
        rulesEngine.registerRule(this._ocenakandydata_24);
        _ocenakandydata_25 = new ocenakandydata_25(this);
        rulesEngine.registerRule(this._ocenakandydata_25);
        _ocenakandydata_26 = new ocenakandydata_26(this);
        rulesEngine.registerRule(this._ocenakandydata_26);
        _ocenakandydata_27 = new ocenakandydata_27(this);
        rulesEngine.registerRule(this._ocenakandydata_27);
        _ocenakandydata_28 = new ocenakandydata_28(this);
        rulesEngine.registerRule(this._ocenakandydata_28);
        _ocenakandydata_29 = new ocenakandydata_29(this);
        rulesEngine.registerRule(this._ocenakandydata_29);
        _ocenakandydata_30 = new ocenakandydata_30(this);
        rulesEngine.registerRule(this._ocenakandydata_30);
        _ocenakandydata_31 = new ocenakandydata_31(this);
        rulesEngine.registerRule(this._ocenakandydata_31);
        _ocenakandydata_32 = new ocenakandydata_32(this);
        rulesEngine.registerRule(this._ocenakandydata_32);
        _ocenakandydata_33 = new ocenakandydata_33(this);
        rulesEngine.registerRule(this._ocenakandydata_33);
        _ocenakandydata_34 = new ocenakandydata_34(this);
        rulesEngine.registerRule(this._ocenakandydata_34);
        _ocenakandydata_35 = new ocenakandydata_35(this);
        rulesEngine.registerRule(this._ocenakandydata_35);
        _ocenakandydata_36 = new ocenakandydata_36(this);
        rulesEngine.registerRule(this._ocenakandydata_36);
        _ocenakandydata_37 = new ocenakandydata_37(this);
        rulesEngine.registerRule(this._ocenakandydata_37);
        _ocenakandydata_38 = new ocenakandydata_38(this);
        rulesEngine.registerRule(this._ocenakandydata_38);
        _ocenakandydata_39 = new ocenakandydata_39(this);
        rulesEngine.registerRule(this._ocenakandydata_39);
        _ocenakandydata_40 = new ocenakandydata_40(this);
        rulesEngine.registerRule(this._ocenakandydata_40);
        _ocenakandydata_41 = new ocenakandydata_41(this);
        rulesEngine.registerRule(this._ocenakandydata_41);
        _ocenakandydata_42 = new ocenakandydata_42(this);
        rulesEngine.registerRule(this._ocenakandydata_42);
        _ocenakandydata_43 = new ocenakandydata_43(this);
        rulesEngine.registerRule(this._ocenakandydata_43);
        _ocenakandydata_44 = new ocenakandydata_44(this);
        rulesEngine.registerRule(this._ocenakandydata_44);
        _ocenakandydata_45 = new ocenakandydata_45(this);
        rulesEngine.registerRule(this._ocenakandydata_45);
        _ocenakandydata_46 = new ocenakandydata_46(this);
        rulesEngine.registerRule(this._ocenakandydata_46);
        _ocenakandydata_47 = new ocenakandydata_47(this);
        rulesEngine.registerRule(this._ocenakandydata_47);
        _ocenakandydata_48 = new ocenakandydata_48(this);
        rulesEngine.registerRule(this._ocenakandydata_48);
        _ocenakandydata_49 = new ocenakandydata_49(this);
        rulesEngine.registerRule(this._ocenakandydata_49);
        _ocenakandydata_50 = new ocenakandydata_50(this);
        rulesEngine.registerRule(this._ocenakandydata_50);
        _ocenakandydata_51 = new ocenakandydata_51(this);
        rulesEngine.registerRule(this._ocenakandydata_51);
        _ocenakandydata_52 = new ocenakandydata_52(this);
        rulesEngine.registerRule(this._ocenakandydata_52);
        _ocenakandydata_53 = new ocenakandydata_53(this);
        rulesEngine.registerRule(this._ocenakandydata_53);
        _ocenakandydata_54 = new ocenakandydata_54(this);
        rulesEngine.registerRule(this._ocenakandydata_54);
        _ocenakandydata_55 = new ocenakandydata_55(this);
        rulesEngine.registerRule(this._ocenakandydata_55);
        _ocenakandydata_56 = new ocenakandydata_56(this);
        rulesEngine.registerRule(this._ocenakandydata_56);
        _ocenakandydata_57 = new ocenakandydata_57(this);
        rulesEngine.registerRule(this._ocenakandydata_57);
        _ocenakandydata_58 = new ocenakandydata_58(this);
        rulesEngine.registerRule(this._ocenakandydata_58);
        _ocenakandydata_59 = new ocenakandydata_59(this);
        rulesEngine.registerRule(this._ocenakandydata_59);
        _ocenakandydata_60 = new ocenakandydata_60(this);
        rulesEngine.registerRule(this._ocenakandydata_60);
        _ocenakandydata_61 = new ocenakandydata_61(this);
        rulesEngine.registerRule(this._ocenakandydata_61);
        _ocenakandydata_62 = new ocenakandydata_62(this);
        rulesEngine.registerRule(this._ocenakandydata_62);
        _ocenakandydata_63 = new ocenakandydata_63(this);
        rulesEngine.registerRule(this._ocenakandydata_63);
        _ocenakandydata_64 = new ocenakandydata_64(this);
        rulesEngine.registerRule(this._ocenakandydata_64);
 /*DATA*/
    }

    public void printState (){
        System.out.println(_ocenakandydata);
    }


    /**
     * http://stackoverflow.com/questions/18489273/how-to-get-percentage-of-cpu-usage-of-os-from-java
     * @param args
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main (String [] args) throws IOException, InterruptedException {
        new Thread(new Measurer()).start();
        while (cpu == -1) {
            System.out.println("Waiting for measuring thread...");
            Thread.sleep(3000);
        }
        BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt",true));

        Start a=new Start();
        a._ocenakandydata.setOcenaKwalifikacji(5.0);
        a._ocenakandydata.setOcenaRozmowy(2.0);
        a._ocenakandydata.setOcenaTestow(2.0);
        Date start = new Date();
        a.rulesEngine.fireRules();
        Date end = new Date();
        Measurer.ifContinue = false;
        a.printState();

        Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
        long minMem = mem[0];
        long maxMem = mem[0];
        for (long m : mem) {
            if (m < minMem) minMem = m;
            if (m > maxMem) maxMem = m;
        }

        Double [] proc = processorUsage.toArray(new Double[processorUsage.size()]);
        double minProc = proc[0];
        double maxProc = proc[0];
        for (double p : proc) {
            if (p < minProc) minProc = p;
            if (p > maxProc) maxProc = p;
        }
        try {
            bw.write("Mem diff: ");
            bw.write(String.valueOf( (maxMem - minMem) / 1024L));
            bw.write(" KB , Processor diff: ");
            bw.write(String.valueOf(maxProc-minProc));
            bw.write(" % , Time diff: ");
            bw.write(String.valueOf(end.getTime() - start.getTime()));
            bw.write(" ms \n-------\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


