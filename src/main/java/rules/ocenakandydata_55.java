package rules;
import objects.*;
import starts.*;
import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.annotation.Rule;
import java.util.Arrays;
import java.util.Collections;
@Rule(name = "ocenakandydata_55", description = "null")
public class ocenakandydata_55{
public Start start;
public ocenakandydata_55(Start a){start =a;}

@Condition
 public boolean evaluate(){
 
if((start._ocenakandydata.getOcenaRozmowy()==2.0000)&&
(start._ocenakandydata.getOcenaKwalifikacji()==3.0000)&&
(start._ocenakandydata.getOcenaTestow()==4.0000)
){
return true;
}else{
return false;
}
}
@Action
 public void execute() throws Exception{
try{

start._ocenakandydata.setOcenaKandydata(start._ocenakandydata.getDecisionForocenaKandydata55());


}catch(Exception e){
throw new Exception(e); 
}

}
}
