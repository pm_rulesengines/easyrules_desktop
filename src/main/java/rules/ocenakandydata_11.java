package rules;
import objects.*;
import starts.*;
import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.annotation.Rule;
import java.util.Arrays;
import java.util.Collections;
@Rule(name = "ocenakandydata_11", description = "null")
public class ocenakandydata_11{
public Start start;
public ocenakandydata_11(Start a){start =a;}

@Condition
 public boolean evaluate(){
 
if((start._ocenakandydata.getOcenaRozmowy()==5.0000)&&
(start._ocenakandydata.getOcenaKwalifikacji()==3.0000)&&
(start._ocenakandydata.getOcenaTestow()==3.0000)
){
return true;
}else{
return false;
}
}
@Action
 public void execute() throws Exception{
try{

start._ocenakandydata.setOcenaKandydata(start._ocenakandydata.getDecisionForocenaKandydata11());


}catch(Exception e){
throw new Exception(e); 
}

}
}
