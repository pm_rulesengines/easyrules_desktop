package rules;
import objects.*;
import starts.*;
import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.annotation.Rule;
import java.util.Arrays;
import java.util.Collections;
@Rule(name = "ocenakandydata_42", description = "null")
public class ocenakandydata_42{
public Start start;
public ocenakandydata_42(Start a){start =a;}

@Condition
 public boolean evaluate(){
 
if((start._ocenakandydata.getOcenaRozmowy()==3.0000)&&
(start._ocenakandydata.getOcenaKwalifikacji()==4.0000)&&
(start._ocenakandydata.getOcenaTestow()==3.0000)
){
return true;
}else{
return false;
}
}
@Action
 public void execute() throws Exception{
try{

start._ocenakandydata.setOcenaKandydata(start._ocenakandydata.getDecisionForocenaKandydata42());


}catch(Exception e){
throw new Exception(e); 
}

}
}
